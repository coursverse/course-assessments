#!/usr/bin/env bash
#
# This script process a file or set of files using the commandline interface of AMC. It will execute within a project or will create a new one.
#
# One usage of the script is
#  
#   process-files.sh --scans <file> --template <~/.AMC.d/Models/survey.tgz> -m --output-dir </tmp/out/dir>
#
# this one will evaluate <file> and will use the template (by default) <~/.AMC.d/Models/survey.tgz> using multiple copies and will produce the output on </tmp/out/dir>. In case a project exists, one can use it and update it
#
#   process-files.sh --scans <file> --project </path/to/project> -m
#
# Also a list of files can be passed instead of a single file. However, the script needs to process them as a single argument. Thus the list should be wrapped between quotes
#
#   process-files.sh --scans '<file1> <file2> ... <fileN>' --template <~/.AMC.d/Models/survey.tgz> -m --output-dir </tmp/out/dir>
#
#
# Config and setup from:
# https://github.com/kvz/bash3boilerplate/blob/master/main.sh

### Configuration
#####################################################################

# Exit on error. Append ||true if you expect an error.
# `set` is safer than relying on a shebang like `#!/bin/bash -e` because that is neutralized
# when someone runs your script as `bash yourscript.sh`
set -o errexit
set -o nounset

# Bash will remember & return the highest exitcode in a chain of pipes.
# This way you can catch the error in case mysqldump fails in `mysqldump |gzip`
set -o pipefail
# set -o xtrace

# Environment variables and their defaults
LOG_LEVEL="${LOG_LEVEL:-6}" # 7 = debug -> 0 = emergency

# Commandline options. This defines the usage page, and is used to parse cli
# opts & defaults from. The parsing is unforgiving so be precise in your syntax
# - A short option must be preset for every long option; but every short option
#   need not have a long option
# - `--` is respected as the separator between options and arguments
read -r -d '' usage <<-'EOF' || true # exits non-zero when EOF encountered  
  -s --scans         [arg] Location of scans. A list of paths can be passed but they need to be separated by spaces, escaped correctly, and the list need to be between quotes. Required.

  Script Options
  -p --project       [arg] Location of project created with AMC. Default (in case it doesn't exist or is not provided) will be created in current directory using the name of the scan.
  -t --template      [arg] Location of the template installed in AMC templates. Default="$HOME/.AMC.d/Models/survey.tgz"
  -u --survey        [arg] Name of the survey to process. The name should match the one stored in the template. Default="survey.tex"
  -l --list-file     [arg] Name of the list of files. Default="list-files.txt"
  -c --output-dir    [arg] Location of the output directory (in case relative paths are used). It is used to move to that directory. Default="."
  
  AMC Options
  -e --density       [arg] Density of the convertion. Default="300"
  -n --tolerance     [arg] Tolerance bounds when detecting the corners. It can be a single real number or inf,sup for different ranges. Default="0.3,0.2"
  -r --proportion    [arg] Proportion (percentage) on each box to be considered as thicked. Default="0.15"
  -b --bw            [arg] Black and white threhold in the convertion of images to detect the corners. 1 is black and 0 is white. Default="0.6"
  -x --export-format [arg] Export format. It can be ods or CSV. Default="CSV"

  Flags
  -m --multiple            Enable multiple copies (photocopies), otherwise each page is treated as a new one
  -v                       Enable verbose mode, print script as it is executed
  -d --debug               Enables debug mode
  -h --help                This page
EOF

# Set magic variables for current file and its directory.
# BASH_SOURCE[0] is used so we can display the current file even if it is sourced by a parent script.
# If you need the script that was executed, consider using $0 instead.
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__cur_dir="$(pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__os="Linux"
if [[ "${OSTYPE:-}" == "darwin"* ]]; then
  __os="OSX"
fi

### Functions
#####################################################################

function emergency () {                             echo "${@}" 1>&2 || true; exit 1; }
function alert ()     { [ "${LOG_LEVEL}" -ge 1 ] && echo "${@}" 1>&2 || true; }
function critical ()  { [ "${LOG_LEVEL}" -ge 2 ] && echo "${@}" 1>&2 || true; }
function error ()     { [ "${LOG_LEVEL}" -ge 3 ] && echo "${@}" 1>&2 || true; }
function warning ()   { [ "${LOG_LEVEL}" -ge 4 ] && echo "${@}" 1>&2 || true; }
function notice ()    { [ "${LOG_LEVEL}" -ge 5 ] && echo "${@}" 1>&2 || true; }
function info ()      { [ "${LOG_LEVEL}" -ge 6 ] && echo "${@}" 1>&2 || true; }
function debug ()     { [ "${LOG_LEVEL}" -ge 7 ] && echo "${@}" 1>&2 || true; }

function help () {
  echo "" 1>&2
  echo " ${@}" 1>&2
  echo "" 1>&2
  echo "  ${usage}" 1>&2
  echo "" 1>&2
  exit 1
}

# Creates the log passed as parameter, and returns the log line for AMC
# In case no debug is needed, then the lne is empty and no log is created
function create_log_amc() {
	if [ "${LOG_LEVEL}" -ge 7 ]; then 
		# $debug is set
		debugFile="${projectDir}/$1.log"
		touch $debugFile
		debugPar="--debug $debugFile"
	else
	  # $debug is unset
		debugPar=
	fi
}
debugPar=

function cleanup_before_exit () {
	cd ${__cur_dir}
  info "Cleaning up. Done"
}
trap cleanup_before_exit EXIT


### Parse commandline options
#####################################################################

# Translate usage string -> getopts arguments, and set $arg_<flag> defaults
while read line; do
  # fetch single character version of option string
  opt="$(echo "${line}" |awk '{print $1}' |sed -e 's#^-##')"

  # fetch long version if present
  long_opt="$(echo "${line}" |awk '/\-\-/ {print $2}' |sed -e 's#^--##')"
  long_opt_mangled="$(sed 's#-#_#g' <<< $long_opt)"

  # map long name back to short name
  varname="short_opt_${long_opt_mangled}"
  eval "${varname}=\"${opt}\""

  # check if option takes an argument
  varname="has_arg_${opt}"
  if ! echo "${line}" |egrep '\[.*\]' >/dev/null 2>&1; then
    init="0" # it's a flag. init with 0
    eval "${varname}=0"
  else
    opt="${opt}:" # add : if opt has arg
    init=""  # it has an arg. init with ""
    eval "${varname}=1"
  fi
  opts="${opts:-}${opt}"

  varname="arg_${opt:0:1}"
  if ! echo "${line}" |egrep '\. Default=' >/dev/null 2>&1; then
    eval "${varname}=\"${init}\""
  else
    match="$(echo "${line}" |sed 's#^.*Default=\(\)#\1#g')"
    eval "${varname}=\"${match}\""
  fi
done <<< "${usage}"

# Allow long options like --this
opts="${opts}-:"

# Reset in case getopts has been used previously in the shell.
OPTIND=1

# start parsing command line
set +o nounset # unexpected arguments will cause unbound variables
               # to be dereferenced
# Overwrite $arg_<flag> defaults with the actual CLI options
while getopts "${opts}" opt; do
  [ "${opt}" = "?" ] && help "Invalid use of script: ${@} "

  if [ "${opt}" = "-" ]; then
    # OPTARG is long-option-name or long-option=value
    if [[ "${OPTARG}" =~ .*=.* ]]; then
      # --key=value format
      long=${OPTARG/=*/}
      long_mangled="$(sed 's#-#_#g' <<< $long)"
      # Set opt to the short option corresponding to the long option
      eval "opt=\"\${short_opt_${long_mangled}}\""
      OPTARG=${OPTARG#*=}
    else
      # --key value format
      # Map long name to short version of option
      long_mangled="$(sed 's#-#_#g' <<< $OPTARG)"
      eval "opt=\"\${short_opt_${long_mangled}}\""
      # Only assign OPTARG if option takes an argument
      eval "OPTARG=\"\${@:OPTIND:\${has_arg_${opt}}}\""
      # shift over the argument if argument is expected
      ((OPTIND+=has_arg_${opt}))
    fi
    # we have set opt/OPTARG to the short value and the argument as OPTARG if it exists
  fi
  varname="arg_${opt:0:1}"
  default="${!varname}"

  value="${OPTARG}"
  if [ -z "${OPTARG}" ] && [ "${default}" = "0" ]; then
    value="1"
  fi

  eval "${varname}=\"${value}\""
  debug "cli arg ${varname} = ($default) -> ${!varname}"
done
set -o nounset # no more unbound variable references expected

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift


### Switches (like -d for debugmode, -h for showing helppage)
#####################################################################

# debug mode
if [ "${arg_d}" = "1" ]; then
  set -o xtrace
  LOG_LEVEL="7"
fi

# verbose mode
if [ "${arg_v}" = "1" ]; then
  set -o verbose
fi

# help mode
if [ "${arg_h}" = "1" ]; then
  # Help exists with code 1
  help "Help using ${0}"
fi


### Validation (decide what's required for running your script and error out)
#####################################################################

[ -z "${arg_s:-}" ]     && help      "Setting a filename for scans with -s or --scan is required"
[ -z "${LOG_LEVEL:-}" ] && emergency "Cannot continue without LOG_LEVEL. "

# switch to output dir, the cleanup trap will move us back
mkdir --parents ${arg_c} || true
cd ${arg_c}


## AMC parameters
#####################################################################

# density of the scans (default 300)
density=${arg_e}
debug density $density

# tolerance of the detection
# it can be a single real number or inf,sup for different ranges
tolerance=${arg_n}
debug tolerance $tolerance

# proportion of each box to be considered thicked
proportion=${arg_r}
debug proportion $proportion

# black and white convertion threshold (0 white, 1 black)
bwTrheshold=${arg_b}
debug bwTrheshold $bwTrheshold

# whether copies where made (leave unset or false, otherwise it will be considered true)
copies=${arg_m}
debug copies $copies

# export format
exportFormat=${arg_x}	
debug exportFormat $exportFormat

## My parameters init

projectDir=${arg_p}
debug projectDir $projectDir

# template to create the project when needed
template=${arg_t}
debug template $template

# This scripts assumes the encuesta.tex is in the current directory
surveyFile=${arg_u}
debug surveyFile $surveyFile

# hold the list of files
listFileName=${arg_l}
debug listFileName $listFileName

# Scaned files
scanFile=${arg_s}
debug scanFile $scanFile

baseFile=$(basename $scanFile)
baseFile="${baseFile%.*}"


if [ -z ${projectDir} ] && [ ! -e "${template}" ]; then
	emergency "The default template is not installed. You need to specify an already compiled (with AMC) project (--project) or perfom a 'make install' before attempting this again."
fi

# Check if project was given or exists
if [[ -z ${projectDir} || ! "$(ls -A ${projectDir})" ]]; then 
	# $projectDir is unset or empty, lets create it
	debug "${projectDir} doesn't exist. Creating it"
	projectDir=${baseFile}
	mkdir --parents ${projectDir} || true
  debug "Created ${projectDir}"

	# setup the template	
	tar zxf ${template} -C ${projectDir}
	debug "Extracting template"
  
  # process the template
  debug "Processing prepare"
  create_log_amc "prepare"
  auto-multiple-choice prepare \
    --mode s \
    --prefix ${projectDir} \
    --with pdflatex \
    --out-calage positions.xy \
    ${projectDir}/${surveyFile} 
  debug "End processing prepare"

  # extract the data
  debug "Processing layout"
  mkdir ${projectDir}/data || true
  auto-multiple-choice meptex \
    --src ${projectDir}/positions.xy \
    --data ${projectDir}/data    
  debug "End processing layout"
else
	debug "${projectDir} exists and is not empty"
fi

# set files
listFile=${projectDir}/${listFileName}
dataDir=${projectDir}/data
scansDir=${projectDir}/scans
processDir=${projectDir}/process
exportFile=${projectDir}/$baseFile.$exportFormat

# create dirs
mkdir ${scansDir} || true
mkdir ${processDir} || true

## Process
#####################################################################
debug "Extracting pages into ${projectDir}/${scansDir}"

# create list of files to process
touch ${listFile}
for file in ${scanFile}; do
  echo ${file} >> ${listFile}
done

# extract pages
auto-multiple-choice getimages \
  --copy-to ${scansDir} \
  --vector-density ${density} \
  --list ${listFile}
debug "End Extracting pages"

# process images
ls -d -1 ${scansDir}/** > ${listFile}

if [ "${copies}" = "0" ]; then 
	multiple=''
	debug "Processing single values" 	
else 	
	multiple=--multiple
  debug "Processing photocopies"
fi

debug "Analyzing of data"
create_log_amc "analyze"
# analyze the data
auto-multiple-choice analyse \
  --data ${dataDir} \
  --cr ${processDir} \
  ${multiple} \
  --tol-marque ${tolerance} \
  --prop ${proportion} \
  --bw-threshold ${bwTrheshold} \
  --try-three \
  --liste-fichiers ${listFile} \
  ${debugPar} 
debug "End analyzing data"

# grade the scans
debug "Processing grades"
# extract the grades first
auto-multiple-choice prepare \
  --mode b \
  --data ${dataDir} \
  --with pdflatex \
  ${projectDir}/${surveyFile}

# then do the grading
create_log_amc "grades"
auto-multiple-choice note \
  --data ${dataDir} \
  --seuil ${proportion} \
  --notemin 0 \
  --notemax 10 \
  --grain 0.5 --arrondi n \
  --plafond \
  ${debugPar}
debug "End processing grades"
 
# export
info $(pwd)
debug "Processing export"
create_log_amc "export"

if [ "${exportFormat}" = "CSV" ]; then
  opts="--option-out separateur=;"
else
  opts=
fi

touch /tmp/dummy.csv
auto-multiple-choice export \
  --data ${dataDir} \
  --module ${exportFormat} \
  --o ${exportFile} \
  --useall 0 \
  --fich-noms '/tmp/dummy.csv' \
  ${opts} \
  ${debugPar}

rm /tmp/dummy.csv
debug "End processing export"