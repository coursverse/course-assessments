# Can define the .csv here, or through the command line
#   make DATA=data.csv
#
# Other parameters can be passed here:
#   make DATA=data.csv AUTHOR=Alg\'un\ Curso TITLE=Encuesta\ Docente
# Note that special characters need to be scaped (e.g., white spaces and accents)

# Programs
PROCESS-FILE=./process-files.sh
OUT-DIR=.
DOCKER=docker
DOCKER-OPTS=
DOCKER-IMG=adnrv/texlive:adnamc
AMC-DIR=~/.AMC.d/Models/

# Data
#DATA=data.csv
QUESTIONS=questions
TITLE=Course Assesment
AUTHOR=Instituto de Computa\c{c}\~{a}o

TEMPLATE-FILE=survey
SURVEY-FILE=survey
REPORT-FILE=report

AMC-OUT-DIR=/tmp

all: report

draft:
	pdflatex -interaction=nonstopmode $(SURVEY-FILE).tex

report:
ifneq ($(strip $(DATA)),)
	$(eval defdata := \def\argdata{$(DATA)})
else
	$(eval defdata :=)
endif
	pdflatex -interaction=nonstopmode -jobname=report-$(basename $(notdir $(DATA))) -output-directory=$(OUT-DIR) "\RequirePackage[utf8]{inputenc}\def\argtitle{$(TITLE)}\def\argauthor{$(AUTHOR)}$(defdata)\input{$(QUESTIONS).tex}\input{$(REPORT-FILE).tex}"

process:
	$(PROCESS-FILE) --scans '$(DATA)' --template ~/.AMC.d/Models/$(TEMPLATE-FILE).tgz -m --output-dir $(AMC-OUT-DIR)
	# In case we obtain a set of files, just use the first as auto-name
	$(eval base := $(shell echo $(basename $(notdir $(DATA))) | cut -d" " -f1 ))
	echo $(base)
	# override data, in case we are in process-report rule
	$(eval override DATA := $(AMC-OUT-DIR)/$(base)/$(base).CSV)

process-report: process report

template:
	tar -czf $(TEMPLATE-FILE).tgz $(SURVEY-FILE).tex $(QUESTIONS).tex -C AMC options.xml description.xml

install: template
	mv $(TEMPLATE-FILE).tgz $(AMC-DIR)

# Docker replacement rule
# It parses any of these rules within the docker
docker-%:
	$(DOCKER) run --rm --volume `pwd`:/home $(DOCKER-OPTS) $(DOCKER-IMG) \
		make $(subst docker-,,$@)

# We ignore errors with -
clean:
	-rm *.log *.aux *.out *.amc *.blg *.bbl *.synctex.gz

veryclean: clean
	-rm *.pdf