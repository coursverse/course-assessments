# Acerca de

La encuesta contiene preguntas para evaluación docente básica que comprende cuatro campos: profesor, curso, ayudante, y estudiante.

Esta ecuesta esta diseñada para ser usada en conjunto con AMC, y el estilo de la UDP para este paquete ([udpamc](http://giteit.udp.cl/udp/udp-latex)).


# Archivos

* `encuesta.tex`: Es la template que contiene la encuesta y debe importarse desde AMC.
* `questions.tex`: Es la base de datos con las preguntas que se utilizan para crear la encuesta y el reporte.
* `reporte.tex`: Es la plantilla para generar el reporte de la encuesta que está definida en `encuesta.tex`.
* `udpsurvey.cls`: Es la clase que extiende la funcionalidad de `automultiplechoice.sty` y da la apariencia de tabla para la generación de la encuesta. Si se desea utilizar en otra encuesta puede importarse (ver su uso en `encuesta.tex`). (Este paquete ha sido movido al repositorio [udp-latex](http://giteit.udp.cl/udp/udp-latex).)
* `Makefile`: Script con varios usos. Instala la plantilla en el sistema. Construye la salida `reporte-data.pdf` utilizando la salida de la encuesta procesada por AMC (`data.csv`), de manera automática o manual.
* `process-files.sh`: Script que procesa automáticamente los PDFs y genera los datos (`data.csv`) necesarios para generar el reporte. 
* `README.md`: Este documento informativo.

# Pre requisitos

1.  Instalar AMC en su sistema, ver la [documentación oficial](http://home.gna.org/auto-qcm/download.en) para este fin.
2.  Instalar las clases `udpamc.cls` y `udpsurvey.cls`, para esto puede clonar el repositorio [udp-latex](http://giteit.udp.cl/udp/udp-latex) en su texmf home. Por ejemplo, en `~/texmf/tex/latex/udp-latex`. Note que de no existir dicha estructura (`~/texmf/tex/latex/`) debe crearla. Dentro de la última carpeta `latex` usted puede crear carpetas que tengan cualquier nombre para almacenar clases y estilos.

    ```bash
    mkdir -p ~/texmf/tex/latex/
    cd ~/texmf/tex/latex/
    git clone git@giteit.udp.cl:udp/udp-latex.git
    ```

3.  Asegurese que los archivos `udpamc.cls` y `udpsurvey.cls` pueden ser encontrados por su sistema. Sino realizó el paso anterior o lo instaló en un directorio distinto (supongamos `/ruta/instalacion/especial/`), debe crear un link simbólico en `~/texmf/tex/latex`. Por ejemplo puede ejecutar

    ```bash
    mkdir -p ~/texmf/tex/latex/encuesta
    ln -s /ruta/instalacion/especial/udpamc.cls ~/texmf/tex/latex/encuesta/
    ln -s /ruta/instalacion/especial/udpsurvey.cls ~/texmf/tex/latex/encuesta/
    ```

    donde el directorio `encuesta` es el nombre que desea crear dentro el árbol `texmf`.


# Uso

## <a id="instalacion"></a> Instalación de la encuesta
1.  El `Makefile` es capaz de crear una template para AMC e instalarlo en su sistema. El `Makefile` asume el folder por defecto para la instalación de AMC en `~/.AMC.d/Models`. Si usted cambió este directorio debe de actualizar el `Makefile`.

    Para poder instalarlo debe de ejecutar

    ```bash
    make install
    ```

## Preparación de la encuesta
1.  Para generar la encuesta debe de abrir AMC en su sistema
2.  Crear un nuevo proyecto (Project->New)
3.  Escriba el nombre del proyecto y seleccione la opción nuevo proyecto ("New project")
4.  Si instaló la plantilla como explicado en [Instalación de la encuesta](#instalacion)

    1. Seleccione la opcion desde plantilla ("template"), seleccione la plantilla llamada "encuesta" (descripción "Encuesta Docente").

    Sino
    
    1.  Seleccione la opción desde archivo ("file"), navegue en la ventana que se abre hasta donde descargo este repositorio, y seleccione el archivo `encuesta.tex`
    2.  Copie el archivo de preguntas `questions.tex` en el directorio donde creó la encuesta. El directorio por defecto es `~/MC-Projects`, y allí debería de estar la carpeta con el nombre del proyecto que creó.
5.  Proceda a la preparación de un documento común en AMC. 

* Este documento asume que el usuario está familiarizado con el uso de AMC. De no ser el caso, se sugiere revisar la [documentación oficial](http://home.gna.org/auto-qcm/doc.en) y los [foros](http://project.auto-multiple-choice.net/) para obtener ayuda en el uso de AMC.
* Para más información de comandos especiales para la definición de las preguntas vea la [definición](#definicion).

## Toma de la encuesta
1.  Imprima las copias, y proceda a la entrega y llenado de las mismas.

## Postprocesado manual de la encuesta

1.  Escanee las copias, y proceda a la evaluación a través de AMC.
2.  Exporte la salida de las encuestas a un archivo `.csv` utilizando la interfaz de AMC y el proyecto que generó anteriormente. Asumiremos que el nombre del archivo es `data.csv`.
3.  Ejecute 
    
    ```bash
    make DATA=/path/to/data.csv
    ```
    donde `/path/to` es el directorio donde se encuentra el archivo `data.csv`. Esto creará en el directorio actual un archivo llamado `reporte-data.pdf` con el procesamiento de los datos que existen en `data.csv`.

## Postprocesado automático de la encuesta

1.  Escanee las copias, y proceda a la evaluación a través de AMC.
2.  Ejecute 

    ```bash
    make DATA=/path/to/scan.pdf process-report
    ```
    donde `/path/to` es el directorio donde se encuentra el archivo `scan.pdf`. Esto creará en el directorio actual un archivo llamado `reporte-scan.pdf` con el procesamiento de los datos que se extrajeron automáticamente de `scan.pdf`. 

    En el caso general se crea un repositorio temporal en `/tmp` (ver `Makefile`) que puede ser ajustado utilizando la variable `AMC-OUT-DIR` al ejecutar el comando anterior.


# `Makefile`

El makefile también puede construir el reporte utilizando otras opciones básicas. Por ejemplo se puede enviar otros dos parámetros válidos para cambiar el autor y el título del reporte. Por ejemplo:

```bash
make DATA=data.csv AUTHOR=Alg\'un\ Curso TITLE=Encuesta\ Docente
```

Note que debe de escapar los caracteres especiales (tildes y espacios).

Se puede procesar conjuntos de archivos a través de las facilidades que entrega el makefile. Por ejemplo
```bash
for file in `ls -A /path/to/many/pdf/**`; do
  make DATA=$file OUT-DIR=/final/output AMC-OUT-DIR=/tmp/amc/output process-report
done

```
iterará todos los archivos que se encuentren en `/path/to/many/pdf`, los procesará automáticamente a través de AMC generando así un `.csv`, y luego generará un reporte en la carpeta `/final/output/`. Además, todos los proyectos que se generaron y procesaron se encuentran en `/tmp/amc/output` para su reutilización en caso de ser necesaria.

# `process-files.sh`

El script `process-files.sh` realiza los mismos pasos que se ejecutan en la interfaz gráfica de AMC automáticamente. Esto permite la ejecución y procesamiento de muchos archivos escaneados automáticamente.

La funcionalidad básica es
```bash
process-files.sh \
  -m \
  --scans /path/to/scan.pdf \
  --template /not/default/AMC/Models/encuesta-docente.tgz \
  --output-dir /out/dir
```
esto hace que se procese el archivo `/path/to/scan.pdf` considerando que las encuestas fueron fotocopiadas (`-m`), se utilizará la plantilla que se encuentra en la dirección `/not/default/AMC/Models/encuesta-docente.tgz` (note que el script utiliza la dirección por defecto al instalar la plantilla con `make install` en caso de utilizar este parámetro), y toda la salida se generará en el directorio `/out/dir` donde se creará una carpeta del mismo nombre que el archivo que se envió: `scan`.

También es posible enviar una lista de archivos para ser procesados
```bash
process-files.sh \
  -m \
  --scans '/path/to/scan1.pdf /path/to/scan2.pdf /path/to/scan3.pdf' \
  --output-dir /out/dir
```
en este caso se utilizará la plantilla por defecto (`~/.AMC.d/Models/encuesta-docente.tgz`), y se creará el directorio `/out/dir/scan1` con la salida de la ejecución. Para facilitar el procesamiento se utiliza el nombre del primer archivo de la lista. (Note que `make process-report` toma este comportamiento si se utiliza un conjunto de archivos `DATA`.)

Para reutilizar un proyecto existente (localizado en `/path/to/project`) y actualizarlo con la información de un(os) archivo(s) escaneado(s) se puede ejecutar
```bash
process-files.sh \
  -m \
  --scans '/path/to/scan1.pdf /path/to/scan2.pdf /path/to/scan3.pdf' \
  --project /path/to/project \
  --output-dir /out/dir
```

Para más información sobre los parámetros y la funcionalidad del script ejecute `process-files.sh -h`.

# Otras encuestas

La plantilla `udpsurvey.cls` está construida de tal manera que permite la extensión y creación de nuevas encuestas. La encuesta funciona a través del uso de un formulario de opción múltiple (usando [AMC](http://home.gna.org/auto-qcm/index.en)). Para este fin, se pueden definir las preguntas de manera manual dentro del macro `\onecopy` o bien siguiendo implementación automática iterando una lista de preguntas en un macro (ver la implementación `\questions` en `encuesta.tex`).

En general, la iteración de elementos puede ser de cualquier elemento que pueda ser interpretado por `udpsurvey.cls` (definidos utilizando [`pgfkeys`](http://mirrors.ctan.org/graphics/pgf/base/doc/pgfmanual.pdf)).

## <a id="definicion"></a> Definición de preguntas

Para definir una nueva encuesta debe de definir un conjunto de elementos (similar a los contenidas en `questions.tex`), como

```tex
\def\questions{%
  element,
  {
    sub element of list,
    sub element of list,
    ...
    sub element of list
  },
  ...
  element
}
```

Los elementos pueden ser simples o un conjunto de ellos (denotados entre llaves: `{` y `}`).

Los elementos que están implementados en `udpsurvey.cls` son:

* Preguntas. Las preguntas se definen utilizando `question={<id>}{<text>}`. Note que las preguntas necesitan un identificador de `LaTeX` válido y único. Las preguntas deben tener un tipo definido por `type=<type>`. Por defecto las preguntas son de tipo opción múltiple (`oneitem`). Sin embargo, se puede cambiar el tipo agregando opciones que son procesadas dentro de un grupo.
  
  * Opción múltiple: definidas como `type=oneitem`.
  * Abiertas: definidas como `type=openitem`.
  * Opciones de las preguntas. Además del tipo se pueden definir opciones a cada una de las preguntas a través de `options={<option 1>, ..., <option N>}`. Por ejemplo, se puede establecer parámetros de las preguntas abiertas (usando las opciones de AMC): `options={lines=3, dots=true, lineheight=.3cm}`. 
* Secciones. La encuesta puede definir encabezados (o secciones) con un estilo específico. Para ello, se definen como `section=<text>`.
* Texto libre. Se puede agregar texto libre o instrucciones a la encuesta. Este tipo de elementos se define como `text=<text>`.
* Encabezado de opciones. Se puede agregar un encabezado a la tabla (arriba de cualquier fila) para nombrar las opciones existentes. Este puede mostrar solo las opciones si se ejecuta sin opciones o bien agregar texto a la izquierda de éstas: `header[=<text>]` (`=<text>` es opcional). Adicionalmente, puede manipularse el primer encabezado utilizando `first header=<bool>`:  removerse `first header=false`, o colocarse `first header=true` (valor por defecto).
* Comandos. Se pueden ejecutar macros de LaTeX en la ejecución de a lista a través de `exec=<cmd>`.
* Administración del encabezado automático. Se puede colocar o remover el encabezado automáticamente utilizando las opciones `auto header on` y `auto header off`, respectivamente.
